"use strict";
const {EventEmitter} = require('events');

class Student extends EventEmitter {
    constructor(name, surname, born) {
        super();
        this.name = name;
        this.surname = surname;
        this.born = born;
        this.attend = new Array(12);

        let timer = false;
        const setTimer = () => {
            if (timer) {
                clearTimeout(timer)
            }
            timer = setTimeout(() => {
                this.emit('check');
            }, 10000)
        };
// выполниться первой в новом цикле Event Loop,
// в этом цикле мы не сможет запустить событие, так как оно еще не инициализированно
        process.nextTick(() => {
            setTimer();
            this.emit('created');
        });

        this.once('age', () => {
            setTimer();
            let date = new Date();
            const age = date.getFullYear() - this.born;
            this.emit('studentAge', age);
        });

        this.on('present', (present) => {
            setTimer();
            for (var i = 0; i < this.attend.length; i++) {
                if (this.attend[i]) {
                } else {
                    break;
                }
            }
// i индекс прошедшего (последнего) урока.
// элемент массива это объект с двумя свойствами.
// на ивенте  present, преподователь выставляет был ли ученик на уроке (yes/no),
// и создаю пустое свойство result

           this.attend[i] =
                {
                    present: present,
                    result: ''
                };
            this.emit('present_result', this.attend);
        });

        this.on('point', (result) => {
            setTimer();
            for (var i = 0; i < this.attend.length; i++) {
                if (this.attend[i] && this.attend[i].result) {
                } else if (this.attend[i]) {
                    if (this.attend[i].present != 'no') {
                        break;
                    }
                }
            }

            if (this.attend[i]) {
                this.attend[i].result = result;
                this.emit('point_result', this.attend);
            } else {
                setTimeout(() => {
                    this.emit('no_lesson');
                },0);
            }
        });

        this.on('averagePoint', (result_lessons) => {
            var n = 0, summa;
            var result = result_lessons.reduce((sum, current) => {
                if (current.result) {
                    n++;
                    summa = +sum + Number(current.result);
                    return summa;
                } else {
                    return sum;
                }
            }, 0);
            this.emit('average', result / n);
        });
        setTimer();
    }
}

module.exports = Student;


