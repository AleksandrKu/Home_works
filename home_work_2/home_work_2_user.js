"use strict";
let result_lessons = [];

const  Student = require('./home-work_2_class');

const st = new Student( "Sasha", "Ivanov", 1989);
st.on('created', () => {
    console.log(' User created');
});

st.on('check', () => {
    console.log("Timerout expire");
});

setTimeout(() => {
    st.emit('age');
} , 0);

st.on("studentAge", (age) => {
    console.log("Student age", age);
});


st.on('present_result', (lessons) => {
    result_lessons = lessons;
});
st.on('point_result', (lessons) => {
    result_lessons = lessons;
});

st.once('no_lesson', () => {
    console.log("You put grades more than lessons. Be careful!");
});

st.emit('present','yes'); // yes, no
st.emit('point','12');
st.emit('present','yes'); // yes, no
st.emit('point','8');
st.emit('present','no'); // yes, no
st.emit('present','yes'); // yes, no
st.emit('present','yes'); // yes, no
st.emit('present','yes'); // yes, no
st.emit('point','10');
st.emit('point','10');

console.log(result_lessons);

st.on('average', (res) => {
    console.log('Average Point ' + res);
});

st.emit('averagePoint', result_lessons);