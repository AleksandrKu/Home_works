const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const querystring = require('querystring');
const fileType = require('file-type');

const PORT = process.env.PORT || 8080;
const index_html = fs.readFileSync('index.html');
const index = fs.createReadStream(path.join(__dirname, 'index.html'));
const database_file = path.join(__dirname, 'database.txt');
//fs.writeFile(database_file, "", (err) => { if (err) throw err; });
const write_stream = fs.createWriteStream(database_file, {flags: 'a'});

const log_file = path.join(__dirname, "log.txt");
const events = require('events');
const emitter = new events.EventEmitter;

let start_time = '';
let start_dif_time = '';
let end_time = '';
let end_dif_time = '';

function function_time(time) { return time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds(); }

const write_stream_log = fs.createWriteStream(log_file, {flags: 'a'});
emitter.on('write_log', (data) => {

    let string = '';
    end_time = function_time(new Date());
    end_dif_time = new Date().getTime();

    string = "Request: " + start_time + " Response: " + end_time;
    string += " Time: " + (end_dif_time - start_dif_time) / 1000 + " seconds";
    string += data.text + " " + data.image;
    string += " statusCode: " + data.statusCode;
    string += " userAgent: " + data.userAgent;


    write_stream_log.write(string + "\n");
});
emitter.on('start_time', () => {
    start_dif_time = new Date().getTime();
    start_time = function_time(new Date());
});

const allowedContentTypes = [
    "application/x-www-form-urlencoded",
    "application/json",
    "text/plain"
];
const messages = [];
const not_found = function(res) {
    res.writeHead(404);
    res.end("Not found");
};


const server = http.createServer();
// отлавливаю событие отключение сервера , но Ctrl C не отлавливает
server.on('close', () => {
    write_stream.write("\n" + function_time(new Date()) + "Server was closed " + "\n");
    console.log("close");
});

server.on('request', (req, res) => {
    emitter.emit('start_time');
    const reqParams = url.parse(req.url, true);

    if (req.method === "GET") {
        if (reqParams.pathname === "/") {
            console.log(req.method);
            // res.end(index_html);
            index.pipe(res); // upload index.html
        } else if (reqParams.pathname === "/favicon.ico") {
            const stream = fs.createReadStream(path.join(__dirname, 'favicon.png'));
            stream.on("open", () => {
                stream.pipe(res);
            });
        } else if (reqParams.pathname === "/data") {
            if (reqParams.query.id) { // if query is   id=
                let messageNum = parseInt(reqParams.query.id);
                if (messageNum) {
                    messageNum--;
                    if (messageNum >= messages.length) {
                        res.writeHead(404);
                        return res.end("Not found");
                    }
                    res.setHeader("Content-Type", "application/json");
                    res.end(JSON.stringify(messages[messageNum]));
                } else {
                    res.writeHead(404);
                    res.end("Not found");
                }
            } else { // show all records from file database.txt
                let data = fs.readFileSync(database_file, 'utf8', (err) => { if (err) throw err; });
                console.log(data);
                console.log(typeof data);
                res.setHeader("Content-Type", "text/plain");
                res.write("Info from database-file:\n");
                res.write(data);
                res.end();
            }
        } else if (reqParams.pathname === "/images") {
            if (reqParams.query.img) {
                const image_path = path.join(__dirname, "images", reqParams.query.img);
                fs.stat(image_path, (err, stats) => {
                    //fs.access(path.join(__dirname, reqParams.query.img) , fs.constants.R_OK, (err) => {
                    if (err == null && (stats["mode"] & 4)) { // file is readable

                        let file_type;
                        const read_stream = fs.createReadStream(image_path);

                        read_stream.on('open', () => {
                            read_stream.once('data', (chunk) => {
                                file_type = fileType(chunk);
                                if (file_type) {
                                    res.writeHead(200, {"Content-type": file_type.mime});
                                }
                            });
                            read_stream.pipe(res);
                        });
                        read_stream.on('end', () => {
                         console.log(req.headers);
                         emitter.emit('write_log', {text: " Read image: ", image: reqParams.query.img, statusCode: res.statusCode, userAgent : req.headers['user-agent'] })
                        });

                    } else if (err) {
                        const error = (err.code == "ENOENT") ? "No such file" : "Error";
                        console.log(error);
                        not_found(res);
                    }
                    else {
                        not_found(res);
                    }
                });
            } else { not_found(res); }
        } else {
            not_found(res);
        }
    } else if (req.method === "POST") {
        console.log(req.method);
        let data = "";
        if (allowedContentTypes.includes(req.headers['content-type'])) {
            req.on('data', chunk => {
                data += chunk;
            });
            req.on('end', () => {
                let obj;
                if (req.headers['content-type'] === "application/json") {
                    obj = JSON.parse(data);
                }
                else {
                    obj = querystring.parse(data);
                }
                messages.push(obj);
                write_stream.write(function_time(new Date()) + " " + JSON.stringify(obj) + "\n");

                res.setHeader("Content-Type", "application/json");
                res.end(JSON.stringify({message: "Message added successful", _id: messages.length, data: obj}));
            });
        } else {
            res.writeHead(400);
            res.end("Not correct content-type");
        }
    } else { not_found(res); }
});


server.listen(PORT, () => {
    console.log("Server port: ", server.address().port);
});