var http = require("http");

var options = {
    "method": "POST",
    "hostname": [
        "localhost"
    ],
    "port": "3000",
    "path": [
        "messages"
    ],
    "headers": {
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Postman-Token": "c68c26ed-29c5-4caa-9f8f-c98b9d02b5b0"
    }
};

var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function () {
        var body = Buffer.concat(chunks);
        console.log(body.toString());
    });
});

req.write(JSON.stringify({ user: 'vasya2', password: '1233' }));
req.end();