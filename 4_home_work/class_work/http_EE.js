const http = require('http');

const PORT = process.env.PORT || 8080;

const server = http.createServer();
server.on('request', (req, res) => {
    res.end(req.url);
});
server.on('listening', () => {
    console.log("We started on", server.address().port);
});

server.listen( ()=> {
    console.log("2 We started on", server.address().port);
});

const emit = server.emit;
server.on('SomeEE', console.log);
server.emit = function(event) {
    console.log(event);
    emit.apply(this, arguments);
    if(event === "request") this.emit('SomeEE', 'test');
};
