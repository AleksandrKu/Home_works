const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const querystring = require('querystring');

const PORT = process.env.PORT || 8080;

const allowedContentTypes = [
    "application/x-www-form-urlencoded",
    "application/json"
];

const messages = [];

const server = http.createServer();
server.on('request', (req, res) => {
    const reqParams = url.parse(req.url, true);
    console.log(req);
    if(req.method === "GET") {
        if (reqParams.pathname === "/") {
            console.log(req.method);
            res.end(req.url);
        } else if (reqParams.pathname === "/favicon.ico") {
            const stream = fs.createReadStream(path.join(__dirname, 'favicon.png'));
            stream.on("open", () => {
                stream.pipe(res);
            });
        } else if (reqParams.pathname === "/messages") {
            let messageNum = false;
            if(reqParams.query.id){
                messageNum = parseInt(reqParams.query.id);
                if(isNaN(messageNum)) messageNum = false;
                else {
                    messageNum--;
                    if (messageNum >= messages.length) {
                        res.writeHead(404);
                        return res.end("Not found");
                    }
                }
            }
            let data;
            if(messageNum !== false) data = messages[messageNum];
            else data = messages;
            res.setHeader("Content-Type", "application/json");
            res.end(JSON.stringify({message: data}));
        } else {
            res.writeHead(404);
            res.end("Not found");
        }
    } else if (req.method === "POST") {
        console.log(req.method);
        let data = "";
        if(allowedContentTypes.includes(req.headers['content-type'])){
            req.on('data', chunk => {
                data += chunk;
            });
            req.on('end', () => {
                let obj;
                if(req.headers['content-type'] === "application/json") obj = JSON.parse(data);
                else obj = querystring.parse(data);
                messages.push(obj);
                res.setHeader("Content-Type", "application/json");
                res.end(JSON.stringify({message: "Message added successful", _id: messages.length}));
            });
        } else {
            res.writeHead(400);
            res.end("Not correct content-type");
        }
    } else {
        res.writeHead(404);
        res.end("Not found");
    }
});

server.listen(PORT, ()=> {
    console.log("We started on", server.address().port);
});