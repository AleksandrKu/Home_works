const http = require('http');
const fs = require('fs');
const path = require('path');

const fileType = require('file-type');

const PORT = process.env.PORT || 3000;
const filePath = path.join(__dirname, "ports.json");
const filePathLog = path.join(__dirname, "log.txt");
const filePathLogLog = path.join(__dirname, "log_log.txt");

const events = require('events');
const emitter = new events.EventEmitter;
let string = ""; //строка записи в файл log
let log_log = ""; //строка записи в файл log_log
let fail = 0; //количество успешно загруженных файлов
let success = 0; // кол. прерванных загрузок

/*fs.writeFile(filePathLog, "", (err) => { if (err) throw err; });
fs.writeFile(filePathLogLog, "", (err) => { if (err) throw err; });*/

const server = new http.Server();

setInterval(() => {
    let time = new Date();
    let time_log = time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
    console.log(time_log);
    log_log = " Time: " + time_log;
    log_log += " Succes read files: " + success + " ";
    log_log += " Errors: " + fail;
    emitter.emit('write_log', {text: log_log});
}, 60000);



server.on('request', (req, res) => {
    if (req.method == "GET" && req.url == "/") {
        const read_stream = fs.createReadStream(filePath);
        const write_stream = fs.createWriteStream(filePathLog, {flags: 'a'});
        read_stream.once('data', (chunk) => {
            let file_type = fileType(chunk);
            if (file_type) {
                res.writeHead(200, {"Content-type": file_type.mime});
            } else {
                res.writeHead(200, {"Content-type": "application/json"});
            }
        });

        let time_dif_start;
        let time_dif_end;
        read_stream.once('data', () => {
            string = "";
            time_dif_start = new Date().getTime();
            let time = new Date();
            let start = time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
            console.log(start);
            string = filePath + " start: " + start;
        });

        // This will wait until we know the readable stream is actually valid before piping
        read_stream.on('open', function () {
        read_stream.pipe(res);
        });

        read_stream.on('end', () => {
            time_dif_end = new Date().getTime();
            let time = new Date();
            let end = time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
            string += " end: " + end + " Time: " + (time_dif_end - time_dif_start) / 1000 + " seconds";
        });

        res.once('finish', () => {
            string += " statusCode = 200";
            log_log += string + "\n";
            success++;
            emitter.emit('write_time', {text: string});
            console.log(string);
        });

        res.on('close', () => { //закрытие окна браузера
            console.log("Response closed!");
            if (typeof(read_stream.destroy) === "function") {
                read_stream.destroy();
                string += " Aborted by customer";
                fail++;
                emitter.emit('write_time', {text: string});
            }
        });

        emitter.once('write_time', (data) => {
            write_stream.write(data.text + "\n");
         });

    } else {
        res.writeHead(404);
        res.end("Not found");
    }
});

server.listen(PORT, (err) => {
    if (err) {
        return console.log(`something bad happened ${err}`)
    }
    console.log(`Server started on post ${PORT}`);
});