const http = require('http');
const fs = require('fs');
const path = require('path');

const fileType = require('file-type');
const readChunk = require('read-chunk');

const PORT = process.env.PORT || 3000;
const filePath = path.join(__dirname, "IMG_6373.JPG");
const filePathLog = path.join(__dirname, "log.txt");

const events = require('events');
const emitter = new events.EventEmitter;

fs.writeFile(filePathLog, "");
const server = new http.Server();
server.on('request', (req, res) => {

    if (req.method == "GET" && req.url == "/") {
/*       const buffer = readChunk.sync(filePath, 0, 4100);
         console.log(fileType(buffer)); */
        const stream = fs.createReadStream(filePath);
        stream.on('readable', () => {
            console.log(filePath, "is readable");

            let data = stream.read(4100);
            if (data && data != null) {
              let  file_type = fileType(data);
                console.log(data.length);
                stream.destroy();
                if (file_type) {
                    console.log(file_type.mime + "\n");
                    res.writeHead(200, {"Content-type": file_type.mime});
                } else {
                    res.writeHead(200, {"Content-type": "application/json"});
                }
            }
        });
        const read_stream = fs.createReadStream(filePath);

        let time_dif_start;
        let time_dif_end;
        read_stream.once('data', () => {
            time_dif_start = new Date().getTime();
            let time = new Date();
            let start = time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
            console.log(start);
            emitter.emit('write_time', {text: "File: ", time: filePath});
            emitter.emit('write_time', {text: "Start read: ", time: start});
        });

        read_stream.pipe(res);
        read_stream.on('end', () => {
            time_dif_end = new Date().getTime();
            let time = new Date();
            let end = time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
            emitter.emit('write_time', {text: "Finish read: ", time: end});
            emitter.emit('write_time', {
                text: "Recording time: ",
                time: (time_dif_end - time_dif_start) / 1000 + " seconds"
            });
        });

        res.once('finish', () => {
            emitter.emit('write_time', {text: "File send successful", time: ""});
            console.log("File send successful");
        });

        res.on('close', () => { //закрытие окна браузера
            console.log("Response closed!");
            if (typeof(read_stream.destroy) === "function") {
                read_stream.destroy();
                emitter.emit('write_time', {text: "Aborted by customer", time: ""});
            }
        });

        emitter.on('write_time', (data) => {
            console.log(data.text, data.time);
            fs.appendFile(filePathLog, data.text + data.time + "\n", function (error) {
                if (error) throw error; // если возникла ошибка
            });
        });
    } else {
        res.writeHead(404);
        res.end("Not found");
    }
});

server.listen(PORT, (err) => {
    if (err) {
        return console.log(`something bad happened ${err}`)
    }
    console.log(`Server started on post ${PORT}`);
});